class Palindrome
  def self.check(string)
    string.downcase == string.downcase.reverse
  end
end
