require 'json'
require_relative 'palindrome'

def check(event:, context:)
  phrase = event.fetch('phrase')
  if Palindrome.check(phrase.to_s)
    true_message
  else
    false_message
  end
rescue KeyError
  validation_error
end

def true_message
  {
    statusCode: 200,
    body: {
      message: true
    }.to_json
  }
end

def false_message
  {
    statusCode: 200,
    body: {
      message: false
    }.to_json
  }
end

def validation_error
  {
    statusCode: 422,
    body: {
      error: 'Validation',
      message: 'Parameter `phrase` is required'
    }.to_json
  }
end
